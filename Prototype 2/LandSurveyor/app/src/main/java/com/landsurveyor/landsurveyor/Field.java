package com.landsurveyor.landsurveyor;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;
import java.text.DecimalFormat;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.toRadians;
import static java.lang.StrictMath.acos;
import static java.lang.StrictMath.atan2;
import static java.lang.StrictMath.sin;

/**
 * Class to hold the list of MapCoordinates.
 * Contains the area given the MapCoordinates
*/
public class Field {
    public ArrayList<MapCoordinates> fieldCoordinates;
    public double area;
    public String fieldName;
    private static final double EARTH_RADIUS = 6371000;// meters

    public Field(){

        fieldCoordinates = new ArrayList<>();
        area = 0f;
    }

    public void clear()
    {
        fieldCoordinates.clear();
    }

    public void setFieldName(String name)
    {
        fieldName = name;
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public void addCoordinate(double lat, double lon)
    {

        MapCoordinates coordinate = new MapCoordinates(lat, lon);
        fieldCoordinates.add(coordinate);

    }

    public void addCoordinates(ArrayList<MapCoordinates> mapCoordinatesArrayList)
    {
        fieldCoordinates = new ArrayList<>(mapCoordinatesArrayList);
    }


    public String printCoordinates()
    {
        StringBuilder text = new StringBuilder();
        area = getFieldArea();

        // add field size, area, and name
        text.append("s" + fieldCoordinates.size() + " a" + Double.toString(area) + "\n");
        text.append(fieldName + "\n");

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            text.append(fieldCoordinates.get(i).latitude + "," + fieldCoordinates.get(i).longitude + "\n");
        }

        return text.toString();
    }

    public ArrayList<String> getLatString()
    {
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            String lat = "";
            lat = Double.toString(fieldCoordinates.get(i).latitude);
            list.add(lat);
        }

        return list;
    }

    public ArrayList<String> getLngString()
    {
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            String lng = "";
            lng = Double.toString(fieldCoordinates.get(i).longitude);
            list.add(lng);
        }

        return list;
    }

    public double getFieldArea()
    {
        List<Location> locations = new ArrayList<>();

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            Location nLoc = new Location("loc");

            double lat = fieldCoordinates.get(i).latitude;
            double lng = fieldCoordinates.get(i).longitude;

            nLoc.setLatitude(lat);
            nLoc.setLongitude(lng);
            locations.add(nLoc);
        }

        //dividing by 10000 to get the mesurements in hectares.
        area = calculateAreaOfGPSPolygonOnSphereInSquareMeters(locations, EARTH_RADIUS)/10000;

        area = Math.round(area *1000);
        return area/1000;
    }

    public double getArea()
    {
        return area;
    }

    private static double calculateAreaOfGPSPolygonOnSphereInSquareMeters(final List<Location> locations, final double radius) {
        if (locations.size() < 3) {
            return 0;
        }

        final List<Double> listY = new ArrayList<Double>();
        final List<Double> listX = new ArrayList<Double>();
        final List<Double> listArea = new ArrayList<Double>();
        final List<Double> distance = new ArrayList<Double>();
        final List<Double> bearing = new ArrayList<Double>();
        final double latitudeRef = locations.get(0).getLatitude();
        final double longitudeRef = locations.get(0).getLongitude();
        // calculate segment x and y in degrees for each point
        for (int i = 0; i < locations.size()-1; i++) {
            final double latitude = locations.get(i+1).getLatitude();
            final double longitude = locations.get(i+1).getLongitude();
            distance.add(calculateDistance(latitudeRef,longitudeRef,latitude,longitude, radius));
            bearing.add(calculateBearing(latitudeRef,longitudeRef,latitude,longitude));
            final double dis = distance.get(i);
            final double bear = bearing.get(i);

            listY.add(calculateYSegment(bear,dis));
            listX.add(calculateXSegment(bear,dis));
        }

        // calculate areas for each triangle segment
        for (int i = 1; i < listX.size(); i++) {
            final double x1 = listX.get(i - 1);
            final double y1 = listY.get(i - 1);
            final double x2 = listX.get(i);
            final double y2 = listY.get(i);
            listArea.add(calculateAreaInSquareMeters(x1, x2, y1, y2));
        }

        // sum areas of all triangle segments
        double areasSum = 0;
        for (final Double area : listArea) {
            areasSum = areasSum + area;
        }

        // if the shape is a legal shape, calculate the are, if not, output 0.
        if(isLegal(listX,listY)) {
            // get absolute value of area, it can't be negative
            return Math.abs(areasSum);// Math.sqrt(areasSum * areasSum);
        }
        return 0;
    }

    private static Double calculateAreaInSquareMeters(final double x1, final double x2, final double y1, final double y2) {
        return (y1 * x2 - x1 * y2) / 2;
    }

    private static double calculateYSegment(final double bear, final double dis) {
        return cos(bear/180 * Math.PI) * dis;
    }

    private static double calculateXSegment(final double bear, final double dis) {
        return sin(bear/180 * Math.PI) * dis;
    }

    private static double calculateDistance(final double latitudeA, final double  longitudeA,final double latitudeB, final double  longitudeB ,final double radius){
        return radius * acos (sin(latitudeA/180 * Math.PI) * sin(latitudeB/180 * Math.PI) + cos(latitudeA/180 * Math.PI) * cos(latitudeB/180 * Math.PI) * cos((longitudeA-longitudeB)/180 * Math.PI));
    }

    private static double calculateBearing(final double latitudeA, final double  longitudeA,final double latitudeB, final double  longitudeB ){
        double x = cos(latitudeB/180 * Math.PI) * sin((longitudeB-longitudeA)/180 * Math.PI);
        double y = cos(latitudeA/180 * Math.PI) * sin(latitudeB/180 * Math.PI) - sin(latitudeA/180 * Math.PI) * cos(latitudeB/180 * Math.PI) * cos((longitudeB-longitudeA)/180 * Math.PI);
        double b = atan2(x,y);
        return b/Math.PI * 180;
    }

    public static boolean isLegal(List<Double> ListX, List<Double> ListY){

        // when there are 3 markers, there's no possibilities the 2 line cross
        if(ListX.size() == 2){
            return true;
        }

        // when there are 4 markers, 2 possibilities the 2 line cross (01-23, 12-30)
        else if(ListX.size() == 3){
            if(isCross(0, 0, ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2))
                    || isCross(ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2), 0, 0)){
                return false;
            }
            return true;
        }

        // when there are 5 markers, 5 possibilities the 2 line cross (01-23, 01-34, 12-34, 12-40, 12-50, 23-40)
        else if(ListX.size() == 4){
            if(isCross(0, 0, ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2))
                    || isCross(0, 0, ListX.get(0), ListY.get(0), ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3))
                    || isCross(ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3))
                    || isCross(ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(3), ListY.get(3), 0, 0)
                    || isCross(ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3), 0, 0)) {
                return false;
            }
            return true;
        }

        // when there are 6 markers, 9 possibilities the 2 line cross (01-23, 01-34, 01-45, 12-34, 12-45, 12-50, 23-45, 23-50, 34-50)
        else if(ListX.size() == 5){
            if(isCross(0, 0, ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2))
                    || isCross(0, 0, ListX.get(0), ListY.get(0), ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3))
                    || isCross(0, 0, ListX.get(0), ListY.get(0), ListX.get(3), ListY.get(3), ListX.get(4), ListY.get(4))
                    || isCross(ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3))
                    || isCross(ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(3), ListY.get(3), ListX.get(4), ListY.get(4))
                    || isCross(ListX.get(0), ListY.get(0), ListX.get(1), ListY.get(1), ListX.get(4), ListY.get(4), 0, 0)
                    || isCross(ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3), ListX.get(4), ListY.get(4))
                    || isCross(ListX.get(1), ListY.get(1), ListX.get(2), ListY.get(2), ListX.get(4), ListY.get(4), 0, 0)
                    || isCross(ListX.get(2), ListY.get(2), ListX.get(3), ListY.get(3), ListX.get(4), ListY.get(4), 0, 0)){
                return false;
            }
            return true;
        }
        return true;
    }




    public static boolean isCross(final double x1, final double y1, final double x2, final double y2,
                                  final double x3, final double y3, final double x4, final double y4){

        double a1 = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
        double a2 = (x2 - x1) * (y4 - y1) - (x4 - x1) * (y2 - y1);

        double b1 = (x4 - x3) * (y1 - y3) - (x1 - x3) * (y4 - y3);
        double b2 = (x4 - x3) * (y2 - y3) - (x2 - x3) * (y4 - y3);

        if(a1 * a2  <= 0 && b1 * b2 <= 0){
            return true;
        }
        return false;
    }

}
