package com.landsurveyor.landsurveyor;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Handles how the fieldList is displayed into the view
 */
public class FieldAdapter extends ArrayAdapter<Field> {

    private Context mContext;
    private ArrayList<Field> fieldList;

    public FieldAdapter(@NonNull Context context, @LayoutRes ArrayList<Field> list)
    {
        super(context, 0, list);
        mContext = context;
        fieldList = list;
    }

    @Override
    public int getCount() {
        return fieldList.size();
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }


    @Override
    public Field getItem(int position) {
        // TODO Auto-generated method stub
        return fieldList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if (listItem == null)
        {
           listItem = LayoutInflater.from(mContext).inflate(R.layout.list_fielddata, parent, false);

            // Alternate color for each listitem
            if (position % 2 == 1)
            {
                listItem.setBackgroundColor(Color.LTGRAY);
            }
            else
            {
                listItem.setBackgroundColor(Color.WHITE);
            }

            DecimalFormat precision = new DecimalFormat("0.000");

            Field currField = fieldList.get(position);

            // Set TextView for FieldName from currField position
            TextView tvFieldName = listItem.findViewById(R.id.textView_fieldName);
            tvFieldName.setText(currField.getFieldName());

            // Set TextView for FieldArea from currField position
            TextView tvFieldArea = listItem.findViewById(R.id.textView_fieldArea);
            tvFieldArea.setText(Double.toString(currField.getArea()) + " hectares");
            //tvFieldArea.setText(precision.format(currField.getArea()));


            // Set listView for latitude from currField
            LayoutInflater layoutInflater;
            layoutInflater = LayoutInflater.from(mContext);

            ArrayList<String> latList = fieldList.get(position).getLatString();
            ArrayList<String> lngList = fieldList.get(position).getLngString();

            LinearLayout layoutLat = listItem.findViewById(R.id.linLayout_Lat);
            LinearLayout layoutLng = listItem.findViewById(R.id.linLayout_Lng);

            for (int i = 0; i < latList.size(); i++)
            {
                View vLat = layoutInflater.inflate(R.layout.lat, null);
                ((TextView) vLat.findViewById(R.id.textView_lat)).setText(latList.get(i));
                layoutLat.addView(vLat);

                View vLng = layoutInflater.inflate(R.layout.lng, null);
                ((TextView) vLng.findViewById(R.id.textView_lng)).setText(lngList.get(i));
                layoutLng.addView(vLng);
            }

        }

        return listItem;
    }
}
