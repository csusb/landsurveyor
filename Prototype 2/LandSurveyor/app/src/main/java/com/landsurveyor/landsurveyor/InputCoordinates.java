package com.landsurveyor.landsurveyor;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;


/**
 * Fragment to handle manual input of coordinates and name of a field to be added to the list of fields
 */
public class InputCoordinates extends Fragment implements View.OnClickListener {

    private EditText etLat1, etLat2, etLat3, etLat4, etLat5, etLat6;
    private EditText etLng1, etLng2, etLng3, etLng4, etLng5, etLng6;
    private EditText etFieldName;

    private Button btnAdd;

    private FieldsData fieldsData = new FieldsData();
    OnDataPass dataPasser;
    public ArrayList<MapCoordinates> savedCoordinates = new ArrayList<>(); // to store field MapCoordinates to transfer to MapFragment


    public InputCoordinates() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input_coordinates, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setUIViews(view);
    }

    public void setUIViews(View v) {

        etLat1 = v.findViewById(R.id.et_InputFieldLat1);
        etLat2 = v.findViewById(R.id.et_InputFieldLat2);
        etLat3 = v.findViewById(R.id.et_InputFieldLat3);
        etLat4 = v.findViewById(R.id.et_InputFieldLat4);
        etLat5 = v.findViewById(R.id.et_InputFieldLat5);
        etLat6 = v.findViewById(R.id.et_InputFieldLat6);

        etLng1 = v.findViewById(R.id.et_InputFieldLng1);
        etLng2 = v.findViewById(R.id.et_InputFieldLng2);
        etLng3 = v.findViewById(R.id.et_InputFieldLng3);
        etLng4 = v.findViewById(R.id.et_InputFieldLng4);
        etLng5 = v.findViewById(R.id.et_InputFieldLng5);
        etLng6 = v.findViewById(R.id.et_InputFieldLng6);

        etFieldName = v.findViewById(R.id.et_InputFieldName);

        btnAdd = v.findViewById(R.id.btn_addInputField);
        btnAdd.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == btnAdd)
        {
            // validate inputs
            boolean validInputs = true;
            boolean[] validPairs = new boolean[6];
            int numValidPairs = 0;
            boolean validNumbers = true;

            String[] latStr = new String[6];
            String[] lngStr = new String[6];

            latStr[0] = etLat1.getText().toString();
            latStr[1] = etLat2.getText().toString();
            latStr[2] = etLat3.getText().toString();
            latStr[3] = etLat4.getText().toString();
            latStr[4] = etLat5.getText().toString();
            latStr[5] = etLat6.getText().toString();

            lngStr[0] = etLng1.getText().toString();
            lngStr[1] = etLng2.getText().toString();
            lngStr[2] = etLng3.getText().toString();
            lngStr[3] = etLng4.getText().toString();
            lngStr[4] = etLng5.getText().toString();
            lngStr[5] = etLng6.getText().toString();

            // check if each pair input is valid
            for (int i = 0; i < 6; i++)
            {
                // at least one is empty
                if (latStr[i].isEmpty() || lngStr[i].isEmpty())
                {
                    // both empty
                    if (latStr[i].isEmpty() && lngStr[i].isEmpty())
                    {
                        validPairs[i] = false;
                    }
                    // one has input, while the other is empty -> invalid
                    else
                    {
                        validPairs[i] = false;
                        validInputs = false;
                    }
                }
                // both have inputs
                else
                {
                    validPairs[i] = true;
                    numValidPairs++;
                }
            }

            ArrayList<MapCoordinates> coordinates = new ArrayList<>();

            if (validInputs && numValidPairs >= 3)
            {
                Toast.makeText(getContext(), "Valid", Toast.LENGTH_SHORT).show();
                // validate if numbers are within lat & lng range
                // -90 < lat < 90
                // -180 < lng < 180

                coordinates.clear();

                for (int i = 0; i < 6; i++)
                {
                    // only check valid pairs
                    if (validPairs[i])
                    {
                        double lat = Double.parseDouble(latStr[i]);
                        double lng = Double.parseDouble(lngStr[i]);

                        if (lat < -90 || lat > 90 || lng < -180 || lng > 180)
                        {
                            validNumbers = false;
                            break;
                        }
                        else
                        {
                            coordinates.add(new MapCoordinates(lat, lng));
                        }
                    }
                }
            }
            else
            {
                Toast.makeText(getContext(), "Minimum 3 valid points", Toast.LENGTH_SHORT).show();
            }

            // check if all numbers are in valid coordinate range
            if (!validNumbers)
            {
                Toast.makeText(getContext(), "Invalid Range", Toast.LENGTH_SHORT).show();
            }

            // check if fieldName is empty
            String fieldName = etFieldName.getText().toString();
            if (fieldName.isEmpty())
            {
                validInputs = false;
                Toast.makeText(getContext(), "Enter Field Name", Toast.LENGTH_SHORT).show();
            }

            // all input are validated ->
            if (validInputs && validNumbers && numValidPairs >= 3)
            {

                Field nField = new Field();
                nField.setFieldName(fieldName);
                nField.addCoordinates(coordinates);
                fieldsData.addField(nField);

                // save to text
                MapFragment mapFragment = (MapFragment) getActivity().getSupportFragmentManager().findFragmentByTag("MapFragment");
                mapFragment.save(getView(), fieldsData.printFieldsData());

                // pass coordinates to activity
                dataPasser.onDataPassFieldsData(fieldsData);
                Toast.makeText(getContext(), "Field Added", Toast.LENGTH_SHORT).show();

                // transfer savedcoordinates and add markers on mapFragment
                savedCoordinates.clear();
                savedCoordinates = coordinates;
                mapFragment.AddMarkersOnSavedCoordinates(savedCoordinates);

                // go back to mapFragment
                ((MainActivity)getActivity()).PopTillFirstFragment();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    @Override
    public void onResume() {
        fieldsData = ((MainActivity) Objects.requireNonNull(getActivity())).getFieldsData();
        super.onResume();
    }
}

