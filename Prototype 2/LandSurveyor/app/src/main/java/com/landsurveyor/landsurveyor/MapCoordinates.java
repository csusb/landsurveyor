package com.landsurveyor.landsurveyor;

/**
 * Class to hold the data pair (latitude and longitude)
 */
public class MapCoordinates {

    public final double latitude;
    public final double longitude;

    public MapCoordinates(double lat, double lon) {
        latitude = lat;
        longitude = lon;
    }
}