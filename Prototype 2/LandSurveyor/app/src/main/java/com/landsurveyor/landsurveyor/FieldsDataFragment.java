package com.landsurveyor.landsurveyor;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;


/**
 * Fragment to handle displaying the fieldsData and OnClick events for each field
 */
public class FieldsDataFragment extends Fragment {

    private FieldsData fieldsData = new FieldsData();;
    private ListView listView;
    private FieldAdapter fieldAdapter;

    public ArrayList<MapCoordinates> savedCoordinates = new ArrayList<>(); // to store field MapCoordinates to transfer to MapFragment

    OnDataPass dataPasser;

    public FieldsDataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fields_data, container, false);
        fieldsData = ((MainActivity) Objects.requireNonNull(getActivity())).getFieldsData();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setUIViews(view);
    }

    public void setUIViews(View v) {

        // if fieldsList is empty, don't populate the view
        if (fieldsData.fieldsList.isEmpty())
        {
            return;
        }

        listView = v.findViewById(R.id.listViewFields);
        fieldAdapter = new FieldAdapter(getActivity(), fieldsData.fieldsList);
        listView.setAdapter(fieldAdapter);

        // Clicking on a field item will open up the MapFragment with the markers of this item
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // store selected item mapCoordinates to local var savedCoordinates
                savedCoordinates.clear();
                savedCoordinates = fieldsData.fieldsList.get(position).fieldCoordinates;

                // transfer savedcoordinates and add markers on mapFragment
                MapFragment fragment = (MapFragment) getActivity().getSupportFragmentManager().findFragmentByTag("MapFragment");
                fragment.AddMarkersOnSavedCoordinates(savedCoordinates);
                // dataPasser.onDataPassMapCoordinates(savedCoordinates);    // transfer to MainActivity
                // press the back button to return to MapFragment
                getActivity().onBackPressed();
            }
        });

        // TODO: Long Clicking on a field item will open up the confirm delete field dialog

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // open confirm delete field dialog

                final AlertDialog dialogBuilder = new AlertDialog.Builder(getContext()).create();
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.confirm_delete_field_dialog, null);

                final TextView textViewFieldName = dialogView.findViewById(R.id.tvDeleteFieldName);  // textView containing the Field Name to be deleted
                textViewFieldName.setText(fieldsData.fieldsList.get(position).getFieldName());  // set to selected FieldName

                Button btnConfirm = dialogView.findViewById(R.id.buttonConfirmDeleteField);
                Button btnCancel = dialogView.findViewById(R.id.buttonCancelDeleteField);

                // Confirm delete
                btnConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // delete field + go to map
                        fieldsData.deleteField(position);
                        dialogBuilder.dismiss();
                        // go back to mapFragment
                        ((MainActivity)getActivity()).PopTillFirstFragment();
                    }
                });

                // Cancel
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                });

                dialogBuilder.setView(dialogView);
                dialogBuilder.show();

                return true;
            }
        });

    }

    // Returns a FieldsData for testing purposes
    public FieldsData testFieldsData()
    {
        FieldsData fieldsData = new FieldsData();

        Field field1 = new Field();
        field1.addCoordinate(1.1,-1.2);
        fieldsData.addField(field1);

        Field field2 = new Field();
        field2.addCoordinate(2.1,-2.2);
        field2.addCoordinate(3.1, 2.2);
        fieldsData.addField(field2);

        return fieldsData;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    @Override
    public void onStop() {
        super.onStop();
        // Transfer fieldsData to MainActivity
        dataPasser.onDataPassFieldsData(fieldsData);
    }

    @Override
    public void onResume() {
        // Get Fields Data from MainActivity whenever this fragment is visible
        fieldsData = ((MainActivity) Objects.requireNonNull(getActivity())).getFieldsData();
        //Toast.makeText(getContext(), fieldsData.fieldsList.get(0).getLatString().get(0), Toast.LENGTH_SHORT).show();

        super.onResume();
    }


}
