package com.landsurveyor.landsurveyor;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class containing an array list of Fields.
 * Handles how the list of fields are printed out to be saved on the text file
 */
public class FieldsData implements Serializable {
    public ArrayList<Field> fieldsList;

    public FieldsData() {
        fieldsList = new ArrayList<>();
    }

    public void addField(Field field)
    {
        fieldsList.add(field);
    }

    public void clearFields() {
        fieldsList.clear();
    }

    public int getSize()
    {
        return fieldsList.size();
    }

    public void deleteField(int position)
    {
        fieldsList.remove(position);
    }

    public String printFieldsData()
    {
        StringBuilder text = new StringBuilder();

        text.append("num fields = " + fieldsList.size() + "\n");

        for (int i = 0; i < fieldsList.size(); i++)
        {

            text.append("f" + i + " ");
            text.append(fieldsList.get(i).printCoordinates());

        }

        return text.toString();
    }


    // Populate the fieldsList from the text
    public void loadFromText(String text)
    {
        // Once text file is loaded to String text, fill in fieldsData from the stringbuilder sb
        if (!text.isEmpty()) {
            fieldsList.clear();

            // Store each line in text as a string array
            String[] lines = text.split("\n");

            // Get FieldDataSize from the first line
            String fieldSizeString = lines[0].replaceAll("[^0-9]+", "");
            int numFields = Integer.parseInt(fieldSizeString);

            int lineIndex = 0;

            for (int i = 0; i < numFields; i++)
            {
                Field newField = new Field();

                // Get size of each field
                String fieldInfo = lines[lineIndex + 1].replaceAll("[^0-9]+", " ");
                // contains header info for each field [field#, fieldSize, fieldArea]
                String[] fieldInfoArr = fieldInfo.trim().split(" ");
                int currFieldSize = Integer.parseInt(fieldInfoArr[1]);
                double currFieldArea = Double.parseDouble(fieldInfoArr[2]);

                String fieldName = lines[lineIndex + 2];

                newField.area = currFieldArea;
                newField.fieldName = fieldName;

                // Get coordinates for each field
                for (int j = lineIndex + 3; j < lineIndex + 3 + currFieldSize; j++)
                {
                    lines[j] = lines[j].replaceAll("[^0-9.-]", " ");
                    String[] LatLngStringArr = lines[j].trim().split(" ");

                    Double lat = Double.parseDouble(LatLngStringArr[0]);
                    Double lng = Double.parseDouble(LatLngStringArr[1]);

                    newField.addCoordinate(lat, lng);
                }

                lineIndex += currFieldSize + 2;

                // add newly created field to fieldList
                fieldsList.add(newField);
            }

        }
    }

}
