package com.landsurveyor.landsurveyor;

import java.util.ArrayList;

public interface OnDataPass {
    void onDataPassFieldsData(FieldsData fieldsData);
    void onDataPassMapCoordinates(ArrayList<MapCoordinates> mapCoordinates);
}
