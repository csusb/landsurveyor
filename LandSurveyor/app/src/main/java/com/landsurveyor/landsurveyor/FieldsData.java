package com.landsurveyor.landsurveyor;


import android.content.Context;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FieldsData implements Serializable {
    public ArrayList<Field> fieldsList;

    public FieldsData() {
        fieldsList = new ArrayList<>();
    }

    public void addField(Field field)
    {
        fieldsList.add(field);
    }

    public void clearFields() {
        fieldsList.clear();
    }

    public int getSize()
    {
        return fieldsList.size();
    }

    public String printFieldsData()
    {
        StringBuilder text = new StringBuilder();

        text.append("num fields = " + fieldsList.size() + "\n");

        for (int i = 0; i < fieldsList.size(); i++)
        {

            text.append("f" + i + " ");
            text.append(fieldsList.get(i).printCoordinates());

        }

        return text.toString();
    }


    public void loadFromText(String text)
    {
        /*TEXT FILE SAMPLE FORMAT

        num fields = 3
        f0 s3
        34.182175,-117.318794
        58.22088431314632,-116.58605843782426
        44.658236504097246,-88.56944616883995
        f1 s3
        50.35414227929132,-116.18437148630618
        62.5815422273092,-86.56101107597351
        39.28410899156292,-120.20565927028655
        f2 s3
        6.8557922196658945,-99.82994668185711
        16.164441432425445,-161.76046270877123
        50.86210966829745,-146.20941683650017
        */

        // Once text file is loaded to String text, fill in fieldsData from the stringbuilder sb
        if (!text.isEmpty()) {
            fieldsList.clear();

            // Store each line in text as a string array
            String[] lines = text.split("\n");

            // Get FieldDataSize from the first line
            String fieldSizeString = lines[0].replaceAll("[^0-9]+", "");
            int numFields = Integer.parseInt(fieldSizeString);

            for (int i = 0; i < numFields; i++)
            {
                Field newField = new Field();

                // Get size of each field
                String fieldInfo = lines[1].replaceAll("[^0-9]+", " ");
                // contains header info for each field [field#, fieldSize, fieldArea]
                String[] fieldInfoArr = fieldInfo.trim().split(" ");
                int currFieldSize = Integer.parseInt(fieldInfoArr[1]);
                double currFieldArea = Double.parseDouble(fieldInfoArr[2]);

                newField.area = currFieldArea;

                // Get coordinates for each field
                for (int j = 2; j < 2 + currFieldSize; j++)
                {
                    lines[j] = lines[j].replaceAll("[^0-9.-]", " ");
                    String[] LatLngStringArr = lines[j].trim().split(" ");

                    Double lat = Double.parseDouble(LatLngStringArr[0]);
                    Double lng = Double.parseDouble(LatLngStringArr[1]);

                    newField.addCoordinate(lat, lng);
                }

                // add newly created field to fieldList
                fieldsList.add(newField);
            }

        }
    }

}
