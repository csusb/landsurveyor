package com.landsurveyor.landsurveyor;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

public class Field {
    public ArrayList<MapCoordinates> fieldCoordinates;
    public double area;
    private static final double EARTH_RADIUS = 6371000;// meters


    public Field(){

        fieldCoordinates = new ArrayList<>();
        area = 0f;
    }

    public void clear()
    {
        fieldCoordinates.clear();
    }

    public void addCoordinate(double lat, double lon)
    {

        MapCoordinates coordinate = new MapCoordinates(lat, lon);
        fieldCoordinates.add(coordinate);

    }

    public void addCoordinates(ArrayList<MapCoordinates> mapCoordinatesArrayList)
    {
        fieldCoordinates = new ArrayList<>(mapCoordinatesArrayList);
    }


    public String printCoordinates()
    {
        StringBuilder text = new StringBuilder();
        area = getFieldArea();

        // add field size and area
        text.append("s" + fieldCoordinates.size() + " a" + Double.toString(area)+ "\n");

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            text.append(fieldCoordinates.get(i).latitude + "," + fieldCoordinates.get(i).longitude + "\n");
        }

        return text.toString();
    }

    public ArrayList<String> getLatString()
    {
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            String lat = "";
            lat = Double.toString(fieldCoordinates.get(i).latitude);
            list.add(lat);
        }

        return list;
    }

    public ArrayList<String> getLngString()
    {
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            String lng = "";
            lng = Double.toString(fieldCoordinates.get(i).longitude);
            list.add(lng);
        }

        return list;
    }

    public double getFieldArea()
    {
        List<Location> locations = new ArrayList<>();

        for (int i = 0; i < fieldCoordinates.size(); i++)
        {
            Location nLoc = new Location("loc");

            double lat = fieldCoordinates.get(i).latitude;
            double lng = fieldCoordinates.get(i).longitude;

            nLoc.setLatitude(lat);
            nLoc.setLongitude(lng);
            locations.add(nLoc);
        }

        //dividing by 10000 to get the mesurements in hectares.
        area = calculateAreaOfGPSPolygonOnSphereInSquareMeters(locations, EARTH_RADIUS)/10000;

        return area;
    }

    public double getArea()
    {
        return area;
    }

    private static double calculateAreaOfGPSPolygonOnSphereInSquareMeters(final List<Location> locations, final double radius) {
        if (locations.size() < 3) {
            return 0;
        }

        final double diameter = radius * 2;
        final double circumference = diameter * Math.PI;
        final List<Double> listY = new ArrayList<Double>();
        final List<Double> listX = new ArrayList<Double>();
        final List<Double> listArea = new ArrayList<Double>();

        // calculate segment x and y in degrees for each point
        final double latitudeRef = locations.get(0).getLatitude();
        final double longitudeRef = locations.get(0).getLongitude();

        for (int i = 1; i < locations.size(); i++) {
            final double latitude = locations.get(i).getLatitude();
            final double longitude = locations.get(i).getLongitude();
            listY.add(calculateYSegment(latitudeRef, latitude, circumference));
            //Log.d(TAG, String.format("Y %s: %s", listY.size() - 1, listY.get(listY.size() - 1)));
            listX.add(calculateXSegment(longitudeRef, longitude, latitude, circumference));
            //Log.d(TAG, String.format("X %s: %s", listX.size() - 1, listX.get(listX.size() - 1)));
        }

        // calculate areas for each triangle segment
        for (int i = 1; i < listX.size(); i++) {
            final double x1 = listX.get(i - 1);
            final double y1 = listY.get(i - 1);
            final double x2 = listX.get(i);
            final double y2 = listY.get(i);
            listArea.add(calculateAreaInSquareMeters(x1, x2, y1, y2));
            //listArea.add(calculateAreaInHectares(x1,x2,y1,y2));
            //Log.d(TAG, String.format("area %s: %s", listArea.size() - 1, listArea.get(listArea.size() - 1)));
        }

        // sum areas of all triangle segments
        double areasSum = 0;
        for (final Double area : listArea) {
            areasSum = areasSum + area;
        }

        // get absolute value of area, it can't be negative
        return Math.abs(areasSum);// Math.sqrt(areasSum * areasSum);
    }

    private static Double calculateAreaInSquareMeters(final double x1, final double x2, final double y1, final double y2) {
        return (y1 * x2 - x1 * y2) / 2;
    }

    /*
    private static Double calculateAreaInHectares(final double x1, final double x2, final double y1, final double y2) {
        //shoelace algorithm divieds by 2 to get m^2, divides by 10000 to get hectares
        return (y1*x2 - x1*y2) / 20000;
    }
    */

    private static double calculateYSegment(final double latitudeRef, final double latitude, final double circumference) {
        return (latitude - latitudeRef) * circumference / 360.0;
    }

    private static double calculateXSegment(final double longitudeRef, final double longitude, final double latitude,
                                            final double circumference) {
        return (longitude - longitudeRef) * circumference * Math.cos(Math.toRadians(latitude)) / 360.0;
    }
}

