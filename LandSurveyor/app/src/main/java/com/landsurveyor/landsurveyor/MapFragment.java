package com.landsurveyor.landsurveyor;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private Button btnClear, btnCalculate, btnSave, btnLoad, btnGetLoc, btnDelete;

    public FieldsData fieldsData;
    public ArrayList<MapCoordinates> savedCoordinates = new ArrayList<>(); // to store field MapCoordinates

    private static final String FILE_NAME = "savedCoordinates.txt";

    OnDataPass dataPasser;

    //creating private value t so that we can keep track of how many markers are on
    //the map.
    private int t = 0;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFragment.getMapAsync(this);

        // Load Up Fields data from txt file
        fieldsData = new FieldsData();

        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setUIViews(view);
        load(view);

    }


    public void setUIViews(View v) {
        btnClear = v.findViewById(R.id.btn_clearMap);
        btnClear.setOnClickListener(this);

        btnCalculate = v.findViewById(R.id.btn_calculate);
        btnCalculate.setOnClickListener(this);

        btnSave = v.findViewById(R.id.btn_saveCoordinates);
        btnSave.setOnClickListener(this);

        btnLoad = v.findViewById(R.id.btn_loadCoordinates);
        btnLoad.setOnClickListener(this);

        btnDelete = v.findViewById(R.id.btn_deleteCoordinates);
        btnDelete.setOnClickListener(this);


        //for gps tracking button on the right to show up, you must allow
        //location services to be turned on.
        //we are using get location button to be able to
        //see where you are in terms of latitude and longitude.
        btnGetLoc = v.findViewById(R.id.btnGetLoc);
        btnGetLoc.setOnClickListener(this);
    }

    /*
        Called when map initially finishes loading
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // set map touch listener
        mMap.setOnMapClickListener(this);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            mMap.setMyLocationEnabled(true);
        }


        /* Terrain types:
            1 - roadmap
            2 - satellite
            3 - hybrid
            4 - terrain
         */
        mMap.setMapType(4);

        // add the markers from the clicked item on FieldsDataFragment
        for (int i = 0; i < savedCoordinates.size(); i++)
        {
            double lat = savedCoordinates.get(i).latitude;
            double lng = savedCoordinates.get(i).longitude;

            mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)));
            t++;
        }


        // Initially mark CSUSB
        /*
        LatLng CSUSB = new LatLng(34.182175, -117.318794);
        mMap.addMarker(new MarkerOptions().position(CSUSB).title("CSUSB"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(CSUSB));   // move camera to CSUSB

        savedCoordinates.add(new MapCoordinates(CSUSB.latitude, CSUSB.longitude));
        */
    }


    /*
        Does something when map is touched on screen
     */
    @Override
    public void onMapClick(LatLng latLng) {
        if (t < 6) {
            // adds a marker on location touched
            mMap.addMarker(new MarkerOptions().position(latLng));
            // add this coordinate on the markedCoordinates list
            savedCoordinates.add(new MapCoordinates(latLng.latitude, latLng.longitude));
        }
        t++;
    }

    /*
        Does something when a view item is clicked
     */
    @Override
    public void onClick(View v) {

        if (v == btnClear) {
            // clear map markers
            mMap.clear();
            savedCoordinates.clear();  // remove all coordinates from the list
            t = 0; //setting t back to 0 to allow for markers again.
        } else if (v == btnCalculate) {
            // calculate area
            Field tempField = new Field();
            tempField.addCoordinates(savedCoordinates);
            Double result = tempField.getFieldArea();
            Toast.makeText(getContext(), result.toString(), Toast.LENGTH_SHORT).show();

        } else if (v == btnSave) {

            // Check if there is at least 3 marked coordinates
            if (savedCoordinates.size() >= 3)
            {
                // Save markedCoordinates into a field and add field to fieldsData
                Field field = new Field();
                field.addCoordinates(savedCoordinates);
                fieldsData.addField(field);

                // write to file
                save(v, fieldsData.printFieldsData());
            }
        } else if (v == btnLoad) {
            load(v);
        } else if (v == btnGetLoc) {
            //Shows longitude and latitude on map with the getLocation button
            GPStracker g = new GPStracker(getContext());
            Location l = g.getLocation();
            if (l != null){
                double lat = l.getLatitude();
                double lng = l.getLongitude();
                Toast.makeText(getContext(), "LAT: " + lat + " \n LNG: " + lng,Toast.LENGTH_LONG).show();
            }
        }
        // delete all data saved in txt file & clear stored data in fieldsData
        else if (v == btnDelete) {
            save(v, "");
            fieldsData.clearFields();
        }
    }


    /*
        Write text to txt file
     */
    public void save(View v, String text) {
        //String text = "Sample text";
        FileOutputStream fos = null;

        try {
            fos = getContext().openFileOutput(FILE_NAME, getContext().MODE_PRIVATE);
            fos.write(text.getBytes());

            Toast.makeText(getContext(), "Saved to" + getContext().getFilesDir() + "/" + FILE_NAME, Toast.LENGTH_SHORT).show();
        }
        catch (FileNotFoundException e) {e.printStackTrace(); }
        catch (IOException e) {e.printStackTrace();}
        finally {
            if (fos != null)
            {
                try {
                    fos.close();
                } catch (IOException e) { e.printStackTrace();}
            }
        }
    }

    /*
        Load to text from txt file
     */
    public void load(View v) {
        FileInputStream fis = null;

        try {
            fis = getContext().openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null)
            {
                sb.append(text).append("\n");
            }

            //Toast.makeText(getContext(),sb, Toast.LENGTH_SHORT).show();

            // Once text file is loaded to String text, fill in fieldsData from the stringbuilder sb
            fieldsData.loadFromText(sb.toString());

        }

        catch (FileNotFoundException e) {e.printStackTrace();}
        catch (IOException e) {e.printStackTrace();}
        finally {
            if (fis != null)
            {
                try {
                    fis.close();
                } catch (IOException e) {e.printStackTrace();}
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    @Override
    public void onResume() {
        // store coordinates from the clicked item from FieldsDataFragment
        savedCoordinates = ((MainActivity) Objects.requireNonNull(getActivity())).getMapCoordinates();
        // then add the markers on the map in the OnMapReady call

        super.onResume();
    }



    @Override
    public void onStop() {
        save(getView(), fieldsData.printFieldsData());
        dataPasser.onDataPassFieldsData(fieldsData);

        super.onStop();
    }
}

