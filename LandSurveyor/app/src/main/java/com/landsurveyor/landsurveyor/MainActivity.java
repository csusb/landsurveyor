package com.landsurveyor.landsurveyor;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        OnDataPass{

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private GoogleMap mMap;

    FieldsData fieldsData = new FieldsData();
    ArrayList<MapCoordinates> mapCoordinates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIViews();

        // Check if newly created activity
        if (savedInstanceState == null) {
            // Start with MapFragment
            getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new MapFragment(), "MapFragment").commit();
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    /*
        Sets all local var to their corresponding UI id in their xml file.
     */
    public void setUIViews() {
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
    }

    /*
        Called when a side menu item is clicked
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        // Adds the selected fragment on top of the current selected fragment
        switch (menuItem.getItemId()) {
            case R.id.nav_tutorial:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new TutorialFragment()).addToBackStack(null).commit();
                break;
            case R.id.nav_about:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new AboutFragment()).addToBackStack(null).commit();
                break;
            case R.id.nav_fieldsData:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new FieldsDataFragment()).addToBackStack(null).commit();
                break;
            case R.id.nav_PlottedPoints:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new PlottedPoints()).addToBackStack(null).commit();
                break;
            case R.id.nav_CalculatePoints:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new CalculatePoints()).addToBackStack(null).commit();
                break;

        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
        Does something when back button is pressed
     */
    @Override
    public void onBackPressed() {
        // closes drawer if opened instead of exiting app or going to the prev fragment
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        // if another fragment is open, back button always return to map
        else if (getSupportFragmentManager().getBackStackEntryCount()>0)
            getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity,new MapFragment(), "MapFragment").addToBackStack(null).commit();
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDataPassFieldsData(FieldsData fieldsData) {
        this.fieldsData = fieldsData;
    }

    @Override
    public void onDataPassMapCoordinates(ArrayList<MapCoordinates> mapCoordinates) {
        this.mapCoordinates = mapCoordinates;
    }

    public FieldsData getFieldsData() {
        return fieldsData;
    }
    public ArrayList<MapCoordinates> getMapCoordinates() { return mapCoordinates;}

}
