
Justin Pulido - Project Manager 

Yousef Jarrar - Assistant Project Manager 

Jameson Dy - Software Engineer (UI/UX)

Sochad Inn - Software Engineer (UI/UX) 

Phillip Pascual - Software Engineer 

Linxuan Lin (Peter) - Software Engineer
 
Robert Richardson - Server Admin 

 
*************************************************************************************************************************

Quality Assurance: 
	Audrey Cards (QA)
 
*************************************************************************************************************************
Graphic Design: 
	Eric Niles (UI/UX Designer)

*************************************************************************************************************************


About this Project: 

The application will be able to locate a minimum of 6 points as for the first portion and we will focus on adding more in the later editions. To be able to calculate the points we are going to have a reference point being the first point, then we are going to continue referencing that point as more markers are dropped. In later editions, we can then detect the missing TCT markers as some are destroyed or misplaced. The second point will reference the first point and so forth. At the end we will have a program that can the have a minimum of 6 points dropped and be able to calculate the area of the land to be able to tell us what the area is after all the geographical changes that occur over the year. As this is important due to the process of purchasing of land when people are promised a lot of land that is X large, but it is smaller than what they were promised. First, we are going to reference the points to point 1 from marker 1…n, then we will measure its perimeter through coordinates or another method of calculations, depending on what may be easier at this given time and still give accurate results. After the calculations are done, we can get a reading as to how big the property is and how it has changed overtime due to geographical changes of the land. Generally, a TCT is a survey that is conducted for the purpose of surveying geographical locations; prior to the development of global positioning system.

*************************************************************************************************************************


         ,----------------,              ,---------,
        ,-----------------------,          ,"        ,"|
      ,"                      ,"|        ,"        ,"  |
     +-----------------------+  |      ,"        ,"    |
     |  .-----------------.  |  |     +---------+      |
     |  |                 |  |  |     | -==----'|      |
     |  |  I LOVE DOS!    |  |  |     |         |      |
     |  |  Bad command or |  |  |/----|`---=    |      |
     |  |  C:\>_          |  |  |   ,/|==== ooo |      ;
     |  |                 |  |  |  // |(((( [33]|    ,"
     |  `-----------------'  |," .;'| |((((     |  ,"
     +-----------------------+  ;;  | |         |,"     
        /_)______________(_/  //'   | +---------+
   ___________________________/___  `,
  /  oooooooooooooooo  .o.  oooo /,   \,"-----------
 / ==ooooooooooooooo==.o.  ooo= //   ,`\--{)B     ,"
/_==__==========__==_ooo__ooo=_/'   /___________,"
`-----------------------------'
