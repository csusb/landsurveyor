package com.landsurveyor.landsurveyor;

import java.util.ArrayList;

/**
 * Interface to help communication between fragments by passing and retrieving data in
 * the MainActivity
 */
public interface OnDataPass {
    void onDataPassFieldsData(FieldsData fieldsData);
    void onDataPassMapCoordinates(ArrayList<MapCoordinates> mapCoordinates);
}
