package com.landsurveyor.landsurveyor;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Fragment to display the map and handles map onClick events.
 * This includes handling the markers and drawing of the polygons on the map.
 * Has the Save Field button which prompts the user to name the field they just created
 * through the number of coordinates marked on the map to be added to the fielsData which will be
 * saved in the text file.
 * Also handles the permissions for user location
 */
public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private Button btnClear, btnCalculate, btnSave, btnLoad, btnGetLoc, btnDelete, btnMark, btnCameraFollow;

    public FieldsData fieldsData = new FieldsData();
    public ArrayList<MapCoordinates> savedCoordinates = new ArrayList<>(); // to store field MapCoordinates

    StringBuilder fieldNameDialog = new StringBuilder();

    //Position of camera
    private static final String FILE_NAME = "FieldsData.txt";

    OnDataPass dataPasser;

    // number of active markers on map
    private int numMarkers = 0;

    // User Location tracker
    private boolean permissionLocation;
    private boolean enableCameraFollow = true;
    private Location lastLocation;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private final long MIN_TIME = 1000; //1 sec
    private final long MIN_DIST = 1; //meters

    private boolean firstStart;

    PolygonOptions polygonOptions=new PolygonOptions(); // for drawing the map trace


    public MapFragment() {
        // Required empty public constructor
    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);
        mapFragment.getMapAsync(this);

        checkLocationPermissions();


        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setUIViews(view);
    }

    // returns true if either of the location permission is enabled
    public boolean checkLocationPermissions()
    {
        if ((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
        {
            //Toast.makeText(getContext(), "Permission denied", Toast.LENGTH_SHORT).show();
            requestPermissions();
            return false;
        }
        else
        {
            //Toast.makeText(getContext(), "Permission granted", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    // Check if Android api location services is on (user location is on)
    public boolean isLocationServiceEnabled(){
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if(locationManager == null)
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        return gps_enabled || network_enabled;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    // returns true if we ask for permissions, otherwise false
    private void requestPermissions()
    {
        // Need to ask for permissions manually for SDK above 23
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // permission not granted -> request permission
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        }

        // SDK below 23
        else
        {
            // will be given during installation
            permissionLocation = true;
        }



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // check if Fine Location & Course Location permissions are granted
        if (requestCode == 100)
        {
/*            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            {
                //Toast.makeText(getContext(), "onrequest granted", Toast.LENGTH_SHORT).show();
                permissionLocation = true;
            }
            else
            {
                //Toast.makeText(getContext(), "onrequest denied", Toast.LENGTH_SHORT).show();
                permissionLocation = false;
            }*/
        }
    }


    public void setUIViews(View v) {
        btnClear = v.findViewById(R.id.btn_clearMap);
        btnClear.setOnClickListener(this);

        btnCalculate = v.findViewById(R.id.btn_calculate);
        btnCalculate.setOnClickListener(this);

        btnSave = v.findViewById(R.id.btn_saveField);
        btnSave.setOnClickListener(this);

        btnLoad = v.findViewById(R.id.btn_loadCoordinates);
        btnLoad.setOnClickListener(this);

        btnDelete = v.findViewById(R.id.btn_deleteCoordinates);
        btnDelete.setOnClickListener(this);

        btnMark = v.findViewById(R.id.btn_markCurrentLocation);
        btnMark.setOnClickListener(this);

        btnCameraFollow = v.findViewById(R.id.btn_cameraFollowOptions);
        btnCameraFollow.setOnClickListener(this);

        //for gps tracking button on the right to show up, you must allow
        //location services to be turned on.
        //we are using get location button to be able to
        //see where you are in terms of latitude and longitude.
        btnGetLoc = v.findViewById(R.id.btnGetLoc);
        btnGetLoc.setOnClickListener(this);
    }

    /*
        Called when map initially finishes loading
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkLocationPermissions())
        {
            mMap.setMyLocationEnabled(true); // show user location (blue dot marker)
        }
        mMap.setMapType(4); // Terrain type map:

        // set map touch listener
        mMap.setOnMapClickListener(this);
        SetLocationListener();

        if(lastLocation != null)
        {
            if (enableCameraFollow)
            {
                moveCamera(lastLocation);
            }
        }
    }


    // returns boolean so other activity knows when it is toggled or not
    public boolean toggleCameraFollow()
    {
        if (enableCameraFollow)
        {
            enableCameraFollow = false;
            return false;
        }
        else
        {
            enableCameraFollow = true;
            return true;
        }
    }

    /*
    Does something when map is touched on screen
 */
    @Override
    public void onMapClick(LatLng latLng) {
        handleAddMarker(latLng);
    }

    @SuppressLint("MissingPermission")
    public void SetLocationListener()
    {
        // user location is on
        if (isLocationServiceEnabled())
        {

            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    lastLocation = location;
                    // check if user wants camera to follow user location
                    if (enableCameraFollow)
                    {
                        moveCamera(location);
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                    if (enableCameraFollow)
                    {
                        lastLocation = locationManager.getLastKnownLocation(provider);
                        moveCamera(lastLocation);
                    }
                }

                @Override
                public void onProviderDisabled(String provider) {
                    requestPermissions();
                }
            };
        }
        else
        {
        }

        locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        // permission already taken cared of in MapFragment
        if (checkLocationPermissions())
        {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME, MIN_DIST, locationListener);
        }
        else
        {
            requestPermissions();
        }

    }

    public void moveCamera(Location location)
    {
        LatLng userLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(userLatLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15)); // adjust zoom
    }

    // Loads up the markers in the map in savedCoordinates
    public void AddMarkersOnSavedCoordinates(ArrayList<MapCoordinates> coordinates)
    {
        ClearMarkers();
        savedCoordinates.clear();

        //Toast.makeText(getContext(), "c: " + Integer.toString(savedCoordinates.size()) + " m: " + Integer.toString(numMarkers), Toast.LENGTH_SHORT).show();

        for (int i = 0; i < coordinates.size(); i++)
        {
            double lat = coordinates.get(i).latitude;
            double lng = coordinates.get(i).longitude;

            handleAddMarker(new LatLng(lat, lng));
        }
        // TODO: Adjust Zoom value depending on the Area of the field selected

    }


    // Clear all markers on map and savedCoordinates
    public void ClearMarkers()
    {
        mMap.clear();        // clear map markers
        numMarkers = 0; //setting t back to 0 to allow for markers again.
        polygonOptions = new PolygonOptions();
    }

    // Handles adding markers on map
    public void handleAddMarker(LatLng latLng)
    {
        if (numMarkers < 6) {
            // adds a marker on location touched
            mMap.addMarker(new MarkerOptions().position(latLng));

            // draw polygon
            drawPolygonMarkers(latLng.latitude, latLng.longitude);

            // add this coordinate on the savedCoordinates list
            savedCoordinates.add(new MapCoordinates(latLng.latitude, latLng.longitude));
            numMarkers++;
        }
        else
        {
            //Toast.makeText(getContext(), "Max of 6 markers", Toast.LENGTH_SHORT).show();
        }
    }

    void drawPolygonMarkers(Double lat, Double lng)
    {
        polygonOptions.add(new LatLng(lat, lng));
        polygonOptions.fillColor(0x7ff68720);
        polygonOptions.strokeColor(0x7ff68720);
        mMap.addPolygon(polygonOptions);
    }

    /*
        Does something when a view item is clicked
     */
    @Override
    public void onClick(View v) {

        if (v == btnClear) {
            ClearMarkers();
            savedCoordinates.clear();  // remove all coordinates from the list
        }
        else if (v == btnCalculate) {
            // calculate area
            Field tempField = new Field();
            tempField.addCoordinates(savedCoordinates);
            Double result = tempField.getFieldArea();
            Toast.makeText(getContext(), result.toString(), Toast.LENGTH_SHORT).show();
        }
        else if (v == btnSave) {

            // Check if there is at least 3 marked coordinates
            if (savedCoordinates.size() >= 3)
            {
                // Show DialogBox to get fieldName & save to file
                FieldNameDialog();
            }
            else
            {
                Toast.makeText(getContext(), "Need at least 3 points", Toast.LENGTH_SHORT).show();
            }
        }
        else if (v == btnLoad) {
            load();
        }
        else if (v == btnGetLoc) {
            //Shows longitude and latitude on map with the getLocation button
            Toast.makeText(getContext(), "LAT: " + Double.toString(lastLocation.getLatitude()) + " \n LNG: " + Double.toString(lastLocation.getLongitude()), Toast.LENGTH_LONG).show();

        }
        else if (v == btnDelete) {
            ClearAllFieldsData();
        }

        // mark user location
        else if (v == btnMark)
        {
            // if permission given
            if (checkLocationPermissions())
            {
                if (lastLocation != null)
                {
                    LatLng latlng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                    handleAddMarker(latlng);
                    moveCamera(lastLocation);
                }
                else
                {
                    //Toast.makeText(getContext(), "Still retrieving location. Make sure to enable user location", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                //Toast.makeText(getContext(), "permission not given", Toast.LENGTH_SHORT).show();
                requestPermissions();
            }

        }
        // enables or disables camera auto follow user location
        else if (v == btnCameraFollow)
        {
            if (enableCameraFollow)
            {
                enableCameraFollow = false;
                btnCameraFollow.setText("Disabled Auto Follow");
            }
            else
            {
                enableCameraFollow = true;
                btnCameraFollow.setText("Enabled Auto Follow");
            }

        }
    }

    // Clears the txt file, fieldsData, markers, and savedCoordinates
    public void ClearAllFieldsData()
    {
        // overwrites saved text file with an empty one; clears the text file
        save(getView(), "");
        fieldsData.clearFields();   // removes all the saved fields in local var
        ClearMarkers();
        savedCoordinates.clear();  // remove all coordinates from the list
    }


    public void SaveToFieldsData(String fieldName)
    {
        // Save markedCoordinates into a field and add field to fieldsData
        Field field = new Field();
        field.fieldName = fieldName;
        field.addCoordinates(savedCoordinates);
        fieldsData.addField(field);

        // write to file
        save(getView(), fieldsData.printFieldsData());
    }

    // Displays a Dialogbox to assign a value to fieldNameDialog
    public void FieldNameDialog()
    {
        final AlertDialog dialogBuilder = new AlertDialog.Builder(getContext()).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fieldname_dialog, null);

        final EditText editTextDialog = dialogView.findViewById(R.id.edt_comment);  // contains the text user types in dialog
        Button btnSubmit = dialogView.findViewById(R.id.buttonSubmit);
        Button btnCancel = dialogView.findViewById(R.id.buttonCancel);

        // submit
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fieldNameDialog = new StringBuilder();

                fieldNameDialog.append(editTextDialog.getText().toString());

                String fName = fieldNameDialog.toString();
                if (!fName.isEmpty())
                {
                    SaveToFieldsData(fieldNameDialog.toString());
                }
                else
                {
                    SaveToFieldsData("Field");
                }
                dialogBuilder.dismiss();
            }
        });

        // cancel
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }


    /*
        Write text to txt file
     */
    public void save(View v, String text) {
        //String text = "Sample text";
        FileOutputStream fos = null;

        try {
            fos = getContext().openFileOutput(FILE_NAME, getContext().MODE_PRIVATE);
            fos.write(text.getBytes());

            if (!text.isEmpty())
            {
                Toast.makeText(getContext(), "Saved to" + getContext().getFilesDir() + "/" + FILE_NAME, Toast.LENGTH_SHORT).show();
            }
        }
        catch (FileNotFoundException e) {e.printStackTrace(); }
        catch (IOException e) {e.printStackTrace();}
        finally {
            if (fos != null)
            {
                try {
                    fos.close();
                } catch (IOException e) { e.printStackTrace();}
            }
        }
    }


    // Populate fieldsData from text file
    public void load() {
        FileInputStream fis = null;

        try {
            fis = getContext().openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null)
            {
                sb.append(text).append("\n");
            }

            // Once text file is loaded to String text, fill in fieldsData from the stringbuilder sb
            fieldsData.loadFromText(sb.toString());
            //Toast.makeText(getContext(),"Loaded from file", Toast.LENGTH_SHORT).show();


        }

        catch (FileNotFoundException e) {e.printStackTrace();}
        catch (IOException e) {e.printStackTrace();}
        finally {
            if (fis != null)
            {
                try {
                    fis.close();
                } catch (IOException e) {e.printStackTrace();}
            }
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
        firstStart = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        fieldsData = ((MainActivity) Objects.requireNonNull(getActivity())).getFieldsData();

        // load from text file only when app first starts
        if (firstStart)
        {
            //Toast.makeText(getContext(), "first start", Toast.LENGTH_SHORT).show();
            load();
            firstStart = false;
        }
        //Toast.makeText(getContext(), Integer.toString(savedCoordinates.size()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // stop listener from updating once service is destroyed/disabled
        if(locationManager != null)
        {
            locationManager.removeUpdates(locationListener);
        }
    }

    @Override
    public void onStop() {
        // Transfer fieldsData to MainActivity
        dataPasser.onDataPassFieldsData(fieldsData);
        //Toast.makeText(getContext(), "sent fieldsData", Toast.LENGTH_SHORT).show();
        super.onStop();
    }
}

