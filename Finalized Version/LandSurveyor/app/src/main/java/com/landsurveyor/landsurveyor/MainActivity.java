package com.landsurveyor.landsurveyor;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;


import java.util.ArrayList;

/**
 *  Contains all the fragments that is displayed on the side navigation drawer.
 *  Handles the navigation drawer.
 *  Has OnDataPass methods which helps fragments communicate data with one another
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        OnDataPass{

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;

    FieldsData fieldsData = new FieldsData();
    ArrayList<MapCoordinates> mapCoordinates = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIViews();

        // Check if newly created activity
        if (savedInstanceState == null) {
            // Start with MapFragment
            getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new MapFragment(), "MapFragment").commit();
        }

        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // Sets all local var to their corresponding UI id in their xml file.
    public void setUIViews() {
        drawerLayout = findViewById(R.id.drawerLayout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
    }

    // Called when a side menu item is clicked
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        // Adds the selected fragment on top of the current selected fragment
        switch (menuItem.getItemId()) {
            case R.id.nav_tutorial:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new TutorialFragment()).addToBackStack(null).commit();
                break;
            case R.id.nav_about:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new AboutFragment()).addToBackStack(null).commit();
                break;
            case R.id.nav_fieldsData:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new FieldsDataFragment()).addToBackStack(null).commit();
                break;
            case R.id.nav_PlottedPoints:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameMainActivity, new InputCoordinates()).addToBackStack(null).commit();
                break;
            case R.id.nav_DeleteSavedData:
                // calls MapFragment's ClearAllFieldsData method
                MapFragment fragment = (MapFragment) getSupportFragmentManager().findFragmentByTag("MapFragment");
                assert fragment != null;
                fragment.ClearAllFieldsData();
                PopTillFirstFragment();
                Toast.makeText(this, "Deleted all data", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_ToggleCameraFollow:
                // calls MapFragment's toggleCameraFollow method
                MapFragment fragment1 = (MapFragment) getSupportFragmentManager().findFragmentByTag("MapFragment");
                assert fragment1 != null;
                boolean toggled = fragment1.toggleCameraFollow();

                // change menu toggle title on whether auto follow is on or not
                if (toggled) {
                    menuItem.setTitle("Disable Camera Auto Follow ");
                    Toast.makeText(this, "Enabled Camera Auto Follow", Toast.LENGTH_SHORT).show();
                } else
                {
                    menuItem.setTitle("Enable Camera Auto Follow ");
                    Toast.makeText(this, "Disabled Camera Auto Follow", Toast.LENGTH_SHORT).show();
                }
                break;

        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    // Does something when back button is pressed
    @Override
    public void onBackPressed() {

        // Get current fragment open
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.frameMainActivity);

        // closes drawer if opened instead of exiting app or going to the prev fragment
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        // if another fragment is open, back button always return to MapFragment
        else if (!(f instanceof MapFragment)) {
            PopTillFirstFragment();
        }
        else {
            super.onBackPressed();
        }
    }

    // remove all prev fragments from BackStack except the first one, which is the MapFragment
    public void PopTillFirstFragment()
    {
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount() + 1; i++) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDataPassFieldsData(FieldsData fieldsData) {
        this.fieldsData = fieldsData;
    }

    @Override
    public void onDataPassMapCoordinates(ArrayList<MapCoordinates> mapCoordinates) {
        this.mapCoordinates = mapCoordinates;
    }

    public FieldsData getFieldsData() {
        return fieldsData;
    }

}
