package com.landsurveyor.landsurveyor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

public class FieldAdapter extends ArrayAdapter<Field> {

    private Context mContext;
    private ArrayList<Field> fieldList;

    public FieldAdapter(@NonNull Context context, @LayoutRes ArrayList<Field> list)
    {
        super(context, 0, list);
        mContext = context;
        fieldList = list;
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;


        if (listItem == null)
        {
           listItem = LayoutInflater.from(mContext).inflate(R.layout.list_fielddata, parent, false);
        }

        // Alternate color for each listitem
        if (position % 2 == 1)
        {
            listItem.setBackgroundColor(Color.LTGRAY);
        }
        else
        {
            listItem.setBackgroundColor(Color.WHITE);
        }

        Field currField = fieldList.get(position);

        // Set TextView for FieldNum from currField position
        TextView tvFieldNum = listItem.findViewById(R.id.textView_fieldNum);
        tvFieldNum.setText(Integer.toString(position));

        // Set TextView for FieldNum from currField position
        TextView tvFieldArea = listItem.findViewById(R.id.textView_fieldArea);
        tvFieldArea.setText(Double.toString(currField.getArea()) + " sq.meters");


        // Set listView for latitude from currField
        /*
        ListView lvLat = listItem.findViewById(R.id.listView_CoordLat);

        ArrayAdapter<String> arrayAdapterLat = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_1,
                currField.getLatString());

        lvLat.setAdapter(arrayAdapterLat);
        */

        LayoutInflater layoutInflater;
        layoutInflater = LayoutInflater.from(mContext);

        ArrayList<String> latList = fieldList.get(position).getLatString();
        ArrayList<String> lngList = fieldList.get(position).getLngString();

        LinearLayout layoutLat = listItem.findViewById(R.id.linLayout_Lat);
        LinearLayout layoutLng = listItem.findViewById(R.id.linLayout_Lng);

        for (int i = 0; i < latList.size(); i++)
        {
            View vLat = layoutInflater.inflate(R.layout.lat, null);
            ((TextView) vLat.findViewById(R.id.textView_lat)).setText(latList.get(i));
            layoutLat.addView(vLat);

            View vLng = layoutInflater.inflate(R.layout.lng, null);
            ((TextView) vLng.findViewById(R.id.textView_lng)).setText(lngList.get(i));
            layoutLng.addView(vLng);
        }


        return listItem;
    }
}
