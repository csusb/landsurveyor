package com.landsurveyor.landsurveyor;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FieldsDataFragment extends Fragment {

    private FieldsData fieldsData;
    private ListView listView;
    private FieldAdapter fieldAdapter;

    public ArrayList<MapCoordinates> savedCoordinates = new ArrayList<>(); // to store field MapCoordinates to transfer to MapFragment

    OnDataPass dataPasser;

    public FieldsDataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fields_data, container, false);
        fieldsData = new FieldsData();
        fieldsData = ((MainActivity) Objects.requireNonNull(getActivity())).getFieldsData();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setUIViews(view);
    }

    public void setUIViews(View v) {
        listView = v.findViewById(R.id.listViewFields);
        fieldAdapter = new FieldAdapter(getActivity(), fieldsData.fieldsList);
        listView.setAdapter(fieldAdapter);

        // Clicking on a field item will open up the MapFragment with the markers of this item
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // store selected item mapCoordinates to local var savedCoordinates
                savedCoordinates =  fieldsData.fieldsList.get(position).fieldCoordinates;
                dataPasser.onDataPassMapCoordinates(savedCoordinates);    // transfer to MainActivity

                // Open MapFragment
                MapFragment mapFragment = new MapFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameMainActivity, mapFragment);
                fragmentTransaction.commit();

            }
        });
    }

    // Returns a FieldsData for testing purposes
    public FieldsData testFieldsData()
    {
        FieldsData fieldsData = new FieldsData();

        Field field1 = new Field();
        field1.addCoordinate(1.1,-1.2);
        fieldsData.addField(field1);

        Field field2 = new Field();
        field2.addCoordinate(2.1,-2.2);
        field2.addCoordinate(3.1, 2.2);
        fieldsData.addField(field2);

        return fieldsData;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
    }

    @Override
    public void onResume() {
        // Get Fields Data from MainActivity whenever this fragment is visible
        fieldsData = ((MainActivity) Objects.requireNonNull(getActivity())).getFieldsData();
        //Toast.makeText(getContext(), fieldsData.fieldsList.get(0).getLatString().get(0), Toast.LENGTH_SHORT).show();

        //fieldAdapter = new FieldAdapter(getActivity(), fieldsData.fieldsList);
        //listView.setAdapter(fieldAdapter);

        super.onResume();
    }


}
