package com.landsurveyor.landsurveyor;

public class MapCoordinates {

    public final double latitude;
    public final double longitude;

    public MapCoordinates(double lat, double lon) {
        latitude = lat;
        longitude = lon;
    }
}